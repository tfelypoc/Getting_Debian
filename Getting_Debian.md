# Getting Debian

This is a short guide explaining how to install and configure Debian like I normally do. It does not cover basic GNU/Linux or Debian knowledge: it is focused on specific topics and it *does* take some things for granted. Some problems can be solved in many ways, but here I only suggest the solutions I prefer. I recommend that you read each section before starting to do what it says. My main sources of information are the [Debian Wiki](https://wiki.debian.org) and the [Arch Wiki](https://wiki.archlinux.org). I decided not to link every article that helped me because this document aims to gather many pieces of information in one place and in a concise, clear way.


## Index

<!-- index.start -->
- [Index](#index)
- [Installing Debian](#installing-debian)
- [Upgrading to Debian Unstable](#upgrading-to-debian-unstable)
- [Disabling APT recommendations and suggestions](#disabling-apt-recommendations-and-suggestions)
- [Getting WiFi and Ethernet to work](#getting-wifi-and-ethernet-to-work)
- [Getting audio to work](#getting-audio-to-work)
- [Configuring the touchpad](#configuring-the-touchpad)
- [Removing default folders from Home](#removing-default-folders-from-home)
- [Disabling recent files](#disabling-recent-files)
- [Changing GRUB background](#changing-grub-background)
- [Quick adjustments](#quick-adjustments)
- [Software recommendations](#software-recommendations)
<!-- index.end -->


## Installing Debian

1. Before starting, backup all the files you want to keep on your new Debian installation. These normally include most of your Home folder, browser bookmarks, emails saved locally and configuration files. You can use an external drive, like a USB stick, or a cloud service. You can directly upload your files there, but it may be more convenient to create a ZIP or TAR archive.

2. Download an installer from the page [Getting Debian](https://www.debian.org/distrib) on the Debian site. There are different options, but you will most likely need a netinst or complete installation image. The former is small in size, but requires an Internet connection during installation, so you will need an Ethernet cable or a similar method of going online without WiFi. The latter is larger, but works completely offline. Make your choice, then select the version that can be recorded onto a removable disk and matches your processor architecture. **Important note:** if you do not have an Ethernet cable and plan to use WiFi on your new system, consider choosing an image with non-free firmware included, as it will be impossible for you to download WiFi drivers later on.

3. Create an installation medium. For this process you will need an external drive. **Please note that all of its content will be overwritten and lost.** If you are already on a GNU/Linux system and want to use a USB stick, the easiest way is plugging in the drive, making sure it is **not** mounted and running the command `sudo cp <path to image> <path to drive>`. For other possibilities, check out [this guide on the Arch Wiki](https://wiki.archlinux.org/title/USB_flash_installation_medium).

4. Power off your computer, then plug in the installation medium and also the computer charger: it is important that the device does not run out of power during the process. If you have chosen a netinst image, connect your Ethernet cable as well.

5. Switch on your computer and open the boot menu before the operating system starts loading. You can do this by pressing a specific key, usually displayed in a message on the screen. The most common ones are `F12`, `F11`, `F10`, `F9`, `F7` and `Esc`. To make sure the key stroke is detected, press the key many times quickly. You should now be able to navigate through a list of devices using the arrow keys: select the installation medium instead of the internal hard disk, then confirm with `Enter`.

6. A screen with the Debian logo and a list of options should appear. You can choose to proceed with a graphical interface or a command-line one. I recommend the latter. At this point you only have to follow the intructions that appear on the screen. I recommend that you also read [this page on the Installation Guide](https://www.debian.org/releases/stable/amd64/ch06s03.en.html) while you go on with the installation. It describes the whole process and explains some important concepts.

### Useful links

- [RFC1178 Choosing a name for your computer](https://www.rfc-editor.org/rfc/rfc1178.html)
- [EFF dice-generated passphrases](https://www.eff.org/dice)

### The desktop environment

I like having a minimal operating system, so during the installation I do not select any desktop environments. Then, since I still want a GUI, I install and configure the X server, a window manager, and all the other programs I need. If you prefer an OS that comes complete with a full desktop experience, you can select a desktop environment. Personally, I like XFCE as it is a good compromise between being good-looking, lightweight and functional.


## Upgrading to Debian Unstable[^1]

To do this, you have to change the respositories from which APT downloads packages. So, open the configuration file `/etc/apt/sources.list` as root. It should contain some lines looking more or less like this:

```
deb http://deb.debian.org/debian/ bullseye main
deb-src http://deb.debian.org/debian/ bullseye main

deb http://security.debian.org/debian-security/ bullseye-security main
deb-src http://security.debian.org/debian-security/ bullseye-security main

deb http://deb.debian.org/debian/ bullseye-updates main
deb-src http://deb.debian.org/debian/ bullseye-updates main
```

In the first two lines, replace the code name of the Debian version ("bullseye" for Debian 11) with "unstable". The following two lines must be deleted, or better, commented, because they are for security updates, which Debian Unstable never gets. The last lines must be deleted or commented as well. Remember to save your changes.

Now run the following commands:

```
sudo apt update
sudo apt full-upgrade
```

Finally, reboot the system (`systemctl reboot`).

### Best practices

- Only upgrade packages when you have some time to fix pontential problems. Do **not** upgrade before important events like a presentation or a business trip: your system will break at the worst times!
- Do **not** blindly accept the actions proposed by the package manager: make sure they are in line with your wishes and expectations.
- Avoid `apt full-upgrade` as it may remove packages you depend on.
- Install the packages `apt-listbugs` and `apt-listchanges`: they will tell you when a package you are about to install or upgrade has bugs or important changes.
- Backup your system frequently.[^2]


## Disabling APT recommendations and suggestions

Create a new empty file in the folder `/etc/apt/apt.conf.d/` and write these lines in it:

```
APT::Install-Recommends "0";
APT::Install-Suggests "0";
```

After doing this, if you want to install a package together with its recommendations and suggestions, you will have to pass the respective flags to APT, namely `--install-recommends` and `--install-suggests`.


## Getting WiFi and Ethernet to work

It is a very common problem that WiFi does not work out of the box after installing Debian. Often, this happens because WiFi needs non-free software, which is not included in the Debian repositories by default.

To install the correct drivers and get WiFi to work, follow [this guide on the Debian Wiki](https://wiki.debian.org/WiFi) and the other guides linked in it. First, you must identify your WiFi device, then install the correct drivers (choose the free ones, if any) and maybe other important packages. After that, I recommend that you disable the non-free repositories, if you had to enable them. This will prevent you from installing more non-free software later. To make sure everything works correctly, reboot your computer.

If you are using a desktop environment, you should now be able to manage WiFi through the preinstalled graphical tools. To do it on the command line, use `nmcli` or `nmtui`, both included in the package `network-manager`.

Note that NetworkManager may have some problems handling wired networks. This is because it does not manage this kind of interfaces by default. To enable them, edit the file `/etc/NetworkManager/NetworkManager.conf` and change the line `managed=false` to `managed=true`. Then, restart NetworkManager with `sudo service NetworkManager restart`.


## Getting audio to work

Sound functionalities in Debian are provided by the Advanced Linux Sound Architecture (ALSA), almost always in combination with a sound server. If you have installed a DE, a sound server like PulseAudio, JACK, or PipeWire probably is already present on your system, and audio should work just fine. If you have done a minimal installation and intend to use ALSA alone, though, you may encounter some complications.

In case the audio does not work right after your minimal installation, install the package `alsa-utils`, then run the command `sudo alsactl init` to initialize ALSA. In most cases, this will do the trick.

If audio does not work yet, the problem may be that your computer has multiple soundcards and Debian by default uses one that is not suitable for playing system sounds. A hint could be `alsamixer`, a handy tool included in `alsa-utils`, not showing any adjustable channels when you launch it. You can see the available soundcards from inside `alsamixer` by pressing `F6`, or you can list them by running `cat /proc/asound/cards` in your terminal. To set a soundcard as the default for your system, create/edit the file `/etc/asound.conf` and write these lines in it, replacing `<n>` with the number of your desired card:

```
defaults.pcm.card <n>
defaults.ctl.card <n>
```

To make sure the new settings are seen by the system, reboot.

You should also know that some programs depend on a sound server to manage sound. For example, without PulseAudio, Firefox used to give me some problems as it did not receive any input from the microphone. Luckily, there is a workaround to this issue that does not use PulseAudio: you can install the package `apulse`, which is basically a partial PulseAudio emulator for ALSA. It allows you to use programs like Firefox simply by launching them with the command `apulse <program>`.

`alsa-utils` also includes other tools to manage audio directly through ALSA. For example, `amixer` is a command-line mixer that will be especially useful if you have to bind keyboard shortcuts to commands in order to adjust the volume.


## Configuring the touchpad

The following instructions assume that the package `xserver-xorg-input-libinput` is installed.

Create/edit the file `/etc/X11/xorg.conf.d/40-libinput.conf` and add/edit the section similar to the following:

```
Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
EndSection
```

You can configure the touchpad by adding the appropriate options, listed in `man libinput`, at the bottom of this section. For example, in order to enable tap-to-click, add the line `Option "Tapping" "on"`. This should also allow you to right-click by tapping with two fingers and to drag by tapping twice.

Finally, for changes to take effect, you will need to restart X.


## Removing default folders from Home

In your Home directory there are some default folders, like `~/Desktop`. If you do not need them, you can safely remove them, but later they might by recreated automatically. To prevent this, run `xdg-user-dirs-update` after deleting the folders you do not need. The command should tell you that some folders were removed, and that the corresponding variables now point to your Home directory.


## Disabling recent files

Set the following options in the file `~/.config/gtk-3.0/settings.ini`:

```
gtk-recent-files-max-age=0
gtk-recent-files-limit=0
gtk-recent-files-enabled=0
```

At this point, it is possible that your file chooser still defaults to the Recent folder. To change this behaviour and set your Home folder as the default, set these options inside `~/.config/gtk-2.0/gtkfilechooser.ini`:

```
StartupMode=cwd
DefaultFolder=cwd
```

The last step is to delete the file `~/.local/share/recently-used.xbel`.


## Changing GRUB background

Open the configuration file `/etc/default/grub` as root. First, set the following options:

```
GRUB_GFXMODE=<screen_width_in_px>x<screen_height_in_px>
GRUB_GFXPAYLOAD_LINUX=keep
```

Then, make sure that the line `GRUB_TERMINAL=console` is absent or commented. Now, choose your new background image. It must have the following properties to work properly:

- PNG, JPG or TGA format;
- resolution equal to that of your screen; this is not essential, but recommended;
- RGB mode, not indexed, 8-bit.

Make also sure that the image is in an unencrypted partition, otherwise GRUB will not be able to read it. Even on encrypted systems, usually `/boot` is unencrypted, so `/boot/grub`, where other configuration files are, is a good choice. Now, you can set the option `GRUB_BACKGROUND="/full/path/to/image"` inside `/etc/default/grub`. Finally, run `sudo update-grub`. If everything went well, the output should contain the line `Found background image: ...`.


## Quick adjustments

This section contains short notes about things you might want to do on Debian. Some of them could be further developed in the future.

- Add the line `export QT_QPA_PLATFORMTHEME=gtk3` to `~/.bashrc` to tell Qt to follow some aspects of GTK.
- You can deactivate Bluetooth in the following ways:
    - `sudo systemctl stop bluetooth.service` stops the Bluetooth unit *now*, but it may start again on the next boot or when manually started;
    - `sudo systemctl disable bluetooth.service` disables the Bluetooth unit: it will not start on boot anymore;
    - setting the option `AutoEnable=false` (this is the default value) in the `[Policy]` section of `/etc/bluetooth/main.conf` will prevent the Bluetooth adapter to power on after a reboot.
- If you want to transfer files between a smartphone and your Debian computer through a USB cable, you may want to install the package `mtp-tools`.
- To use a scanner, install `sane-utils` and then use the command `scanimage`.


## Software recommendations

This is a list of the software I use for different purposes. These are only personal recommendations and if you have installed a DE you probably have most of the programs you need.

- X server: xorg
- Window manager: openbox
- Terminal emulator: xfce4-terminal
- File manager: thunar[^3]
- Image viewer: ristretto
- Image editor: gimp[^4]
- Vector image editor: inkscape
- Media player: parole[^5]
- Video downloader: yt-dlp
- Song metadata editor: exfalso
- Basic video/audio editing: ffmpeg
- Document reader: mupdf
- Document editor: libreoffice-writer[^6][^7]
- IRC client: catgirl
- BitTorrent client: transmission-gtk
- Instant messaging: telegram-desktop
- Web browser: firefox, links (text-mode browser), Tor browser (not in Debian repos)
- Version control system: git
- C++ compiler: g++
- Panels: polybar
- Wallpaper changer: hsetroot
- Screenshots: scrot
- Screen locking tool: xtrlock
- Night light tool: xsct
- Input method: fcitx5[^8]
- Fonts: fonts-hack (interfaces), fonts-freefont-ttf (documents), fonts-noto-cjk (CJK text)


[^1]: Learn more about the releases of Debian:

      - [Debian releases](https://www.debian.org/releases)
      - [Debian releases - Debian Wiki](https://wiki.debian.org/DebianReleases)
      - [Choosing a Debian distribution](https://www.debian.org/doc/manuals/debian-faq/choosing)

      I would like to point out that, despite its name, I have never found Debian Unstable as unstable as the documentation suggests.

[^2]: In the following pages you can find information about backups and many example tools. Personally, I like using TAR.

      - [Backup your system - Ubuntu Community Help Wiki](https://help.ubuntu.com/community/BackupYourSystem)
      - [Backup and recovery - Debian Wiki](https://wiki.debian.org/BackupAndRecovery)

[^3]: Some of Thunar's functionalities, such as the bin, the disk management and file thumbnails, are only available if you install Thunar's recommended packages.

[^4]: If GIMP has an ugly interface, try to launch it from a terminal. If you get this error message:

      ```
      Unable to locate theme engine in module_path: "pixmap"
      ```

      simply install the package `gtk2-engines-pixbuf`.

[^5]: Extra functionalities for Parole, like support for PGS subtitles and extra audio visualisation types, can be added by installing the package `gstreamer1.0-plugins-bad`.

[^6]: If you use LibreOffice, you should also install the package `libreoffice-gtk3`: it will allow LibreOffice to integrate with the system theme.

[^7]: If you want to use a spell checker on LibreOffice, you need the corresponding package. Most of them are `hunspell-...` packages: for example `hunspell-it` is for the Italian spell checker.

[^8]: For `fcitx5` to work in most programs, you need extra packages: make sure you install its recommendations.
